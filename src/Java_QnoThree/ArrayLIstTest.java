package Java_QnoThree;

import java.util.ArrayList;

public class ArrayLIstTest {
  public static void main(String[] args) {
    ArrayList<String> products = new ArrayList<>();
    products.add("Pepsi");
    products.add("Sprite");
    products.add("Fanta");
    products.add("Coke");
    products.add("Mountain Dew");

    System.out.println("Size of the list: " + products.size());
    System.out.println("Contents of the list: " + products);

    products.remove(1);

    System.out.println("Size of the list after removal: " + products.size());
    System.out.println("Contents of the list after removal: " + products);

    if (products.contains("Coke")) {
      System.out.println("Coke is present in the list");
    } else {
      System.out.println("Coke is not present in the list");
    }
  }
}
