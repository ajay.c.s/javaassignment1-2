package Java_QnoTwoE;

public class Area extends Shape {
    @Override
    void rectangleArea(double length, double breadth) {
        System.out.println("Area of rectangle: " + (length * breadth));
    }
    
    @Override
    void squareArea(double side) {
        System.out.println("Area of square: " + (side * side));
    }
    
    @Override
    void circleArea(double radius) {
        System.out.println("Area of circle: " + (Math.PI * radius * radius));
    }
}
