package Java_QnoTwoE;

abstract class Shape {
	abstract void rectangleArea(double length, double breadth);

	abstract void squareArea(double side);

	abstract void circleArea(double radius);
}