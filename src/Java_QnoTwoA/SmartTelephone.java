package Java_QnoTwoA;

public class SmartTelephone extends Telephone {
	@Override
	void dial() {
		System.out.println("Dialing from a SmartTelephone");
	}

	@Override
	void lift() {
		System.out.println("Lifting a SmartTelephone");
	}

	@Override
	void disconnected() {
		System.out.println("Disconnecting a SmartTelephone");
	}
}
