package Java_QnoOne;

interface BasicCar {
	void gearchange();

	void music();
}

class Car {
	void drive() {
		System.out.println("Driving a car");
	}

	void stop() {
		System.out.println("Stopping a car");
	}
}

public class Santro extends Car implements BasicCar {
	void Remotestart() {
		System.out.println("Remote starting Santro");
	}

	@Override
	
	public void gearchange() {
		System.out.println("Changing gears in Santro");
	}

	@Override
	
	public void music() {
		System.out.println("Playing music in Santro");
	}
}
