package Java_QnoOne;

public class Notebook extends Book {
    @Override
    void write() {
        System.out.println("Writing in a notebook");
    }
    
    @Override
    void read() {
        System.out.println("Reading from a notebook");
    }
    
    void draw() {
        System.out.println("Drawing in a notebook");
    }
}
