package Java_qnoTwoB;

public class StringTest {
    public static void main(String[] args) {
        String str = "This is orange juice";
        
        if (str.contains("orange")) {
            System.out.println("The word 'orange' is present in the string.");
        } else {
            System.out.println("The word 'orange' is not present in the string.");
        }
    }
}
