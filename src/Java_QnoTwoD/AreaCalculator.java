package Java_QnoTwoD;

public class AreaCalculator {
	public static void main(String[] args) {
		double rectangleLength = 5;
		double rectangleBreadth = 10;
		double rectangleArea = rectangleLength * rectangleBreadth;
		System.out.println("Area of rectangle: " + rectangleArea);

		double squareSide = 5;
		double squareArea = squareSide * squareSide;
		System.out.println("Area of square: " + squareArea);

		double circleRadius = 5;
		double circleArea = Math.PI * circleRadius * circleRadius;
		System.out.println("Area of circle: " + circleArea);
	}
}
