package Java_QnoTwoC;

public class StringTest2 {
    public static void main(String[] args) {
        String str = "Hello, World";
        
        int firstIndexOfO = str.indexOf('o');
        int lastIndexOfO = str.lastIndexOf('o');
        int indexOfComma = str.indexOf(',');
        
        System.out.println("First occurrence of 'o': " + firstIndexOfO);
        System.out.println("Last occurrence of 'o': " + lastIndexOfO);
        System.out.println("Index of ',': " + indexOfComma);
    }
}
