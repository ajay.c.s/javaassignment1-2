package Java_QnoTwoaa;

public class TV implements TVRemote {
	  @Override
	  public void power() {
	    System.out.println("Turning TV on/off");
	  }
	  
	  @Override
	  public void volumeUp() {
	    System.out.println("Turning volume up");
	  }
	  
	  @Override
	  public void volumeDown() {
	    System.out.println("Turning volume down");
	  }
}