package Java_QnoTwoaa;

class SmartTV extends TV implements SmartTVRemote {
	  @Override
	  public void connectToWiFi() {
	    System.out.println("Connecting to WiFi");
	  }
	  
	  @Override
	  public void accessApps() {
	    System.out.println("Accessing apps");
	  }
	  public static void main(String[] args) {
		SmartTV st =new SmartTV();
		st.connectToWiFi();
		st.accessApps();
		st.power();
		st.volumeDown();
		st.volumeUp();
	}
	}